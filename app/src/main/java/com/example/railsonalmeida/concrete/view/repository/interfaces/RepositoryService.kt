package com.example.railsonalmeida.concrete.view.repository.interfaces

import com.example.railsonalmeida.concrete.view.repository.data.PullRequest
import io.reactivex.Observable
import java.security.acl.Owner

/**
 * Created by railsonalmeida on 19/03/2018.
 */

interface RepositoryService {

    fun loadRepositoryPullRequests(owner: String, name: String): Observable<Array<PullRequest>>

}