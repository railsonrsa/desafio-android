package com.example.railsonalmeida.concrete.view.repository

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.railsonalmeida.concrete.R
import com.example.railsonalmeida.concrete.application.ConcreteApplication
import com.example.railsonalmeida.concrete.application.base.BaseFragment
import com.example.railsonalmeida.concrete.view.repository.adapter.RepositoryAdapter
import com.example.railsonalmeida.concrete.view.repository.data.PullRequest
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryCallback
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryPresenter
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryView
import kotlinx.android.synthetic.main.fragment_repository.*
import java.io.Serializable
import javax.inject.Inject

/**
 * Created by railsonalmeida on 19/03/2018.
 */

class RepositoryFragment: BaseFragment(), RepositoryView {

    @Inject lateinit var presenter: RepositoryPresenter
    private lateinit var callback: RepositoryCallback
    private lateinit var adapter: RepositoryAdapter
    private lateinit var data: ArrayList<PullRequest>

    companion object {
        const val TAG = "REPOSITORY_FRAGMENT"
        const val DATA = "DATA"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (context.applicationContext as ConcreteApplication).repositoryComponent.inject(this)
        callback = context as RepositoryCallback

        if (savedInstanceState != null) {
            data = savedInstanceState.get(DATA) as ArrayList<PullRequest>
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_repository, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setView()
        val repository = callback.getRepository()


        adapter = RepositoryAdapter(context)
        val linearLayoutManager = LinearLayoutManager(this.context)

        rv_pullrequests.adapter = adapter
        rv_pullrequests.layoutManager = linearLayoutManager
        rv_pullrequests.addItemDecoration(DividerItemDecoration(rv_pullrequests.context,
                linearLayoutManager.orientation))

        if (savedInstanceState != null) {
             setPullRequestsData(data)
        } else {
            presenter.loadRepositoryPullRequests(repository)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.destroyDiposables()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putSerializable(DATA, adapter.getAll() as Serializable)
    }

    override fun setView() {
        presenter.setView(this)
    }

    override fun showLoading() {
        loading.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        loading.visibility = View.GONE
    }

    override fun showEmptyPullRequests() {
        tv_empty.visibility = View.VISIBLE
    }

    override fun setPullRequestsData(data: List<PullRequest>) {
        adapter.addAll(data)
        adapter.notifyDataSetChanged()
    }
}