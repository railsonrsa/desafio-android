package com.example.railsonalmeida.concrete.view.repository.di

import com.example.railsonalmeida.concrete.application.schedulers.AppSchedulers
import com.example.railsonalmeida.concrete.application.schedulers.SchedulerProvider
import com.example.railsonalmeida.concrete.view.repository.RepositoryPresenterImpl
import com.example.railsonalmeida.concrete.view.repository.RepositoryServiceImpl
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryApi
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryPresenter
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by railsonalmeida on 19/03/2018.
 */

@Module
class RepositoryModule {

    @Provides
    fun providePresenter(service: RepositoryService, schedulers: SchedulerProvider): RepositoryPresenter {
        return RepositoryPresenterImpl(null, service, schedulers)
    }

    @Provides
    fun provideService(api: RepositoryApi): RepositoryService {
        return RepositoryServiceImpl(api)
    }

    @Provides
    fun provideAppScheduler(): SchedulerProvider {
        return AppSchedulers()
    }

}