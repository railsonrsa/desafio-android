package com.example.railsonalmeida.concrete.view.repository.interfaces

import com.example.railsonalmeida.concrete.view.repository.data.PullRequest

/**
 * Created by railsonalmeida on 19/03/2018.
 */

interface RepositoryView {

    fun showLoading()

    fun hideLoading()

    fun setPullRequestsData(data: List<PullRequest>)

    fun showEmptyPullRequests()

}