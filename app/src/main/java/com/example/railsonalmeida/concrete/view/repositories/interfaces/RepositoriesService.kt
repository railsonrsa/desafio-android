package com.example.railsonalmeida.concrete.view.repositories.interfaces

import com.example.railsonalmeida.concrete.view.repositories.data.Repositories
import io.reactivex.Observable

/**
 * Created by railsonalmeida on 15/03/2018.
 */

interface RepositoriesService {

    fun loadRepositories(page: Int): Observable<Repositories>

}