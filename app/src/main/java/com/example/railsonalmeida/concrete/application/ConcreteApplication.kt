package com.example.railsonalmeida.concrete.application

import android.app.Application
import com.example.railsonalmeida.concrete.application.di.AppComponent
import com.example.railsonalmeida.concrete.application.di.AppModule
import com.example.railsonalmeida.concrete.application.di.DaggerAppComponent
import com.example.railsonalmeida.concrete.view.repositories.di.RepositoriesComponent
import com.example.railsonalmeida.concrete.view.repositories.di.RepositoriesModule
import com.example.railsonalmeida.concrete.view.repository.di.RepositoryComponent
import com.example.railsonalmeida.concrete.view.repository.di.RepositoryModule

/**
 * Created by railsonalmeida on 15/03/2018.
 */

class ConcreteApplication: Application() {

    lateinit var appComponent: AppComponent
    lateinit var repositoriesComponent: RepositoriesComponent
    lateinit var repositoryComponent: RepositoryComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()

        repositoriesComponent = appComponent.inject(RepositoriesModule())
        repositoryComponent = appComponent.inject(RepositoryModule())

    }

}