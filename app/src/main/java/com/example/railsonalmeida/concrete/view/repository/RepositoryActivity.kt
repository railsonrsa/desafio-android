package com.example.railsonalmeida.concrete.view.repository

import android.os.Bundle
import com.example.railsonalmeida.concrete.R
import com.example.railsonalmeida.concrete.application.base.BaseActivity
import com.example.railsonalmeida.concrete.view.repository.data.Repository
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryCallback

class RepositoryActivity : BaseActivity(), RepositoryCallback {

    private lateinit var repository: Repository

    private var repositoryFragment: RepositoryFragment? = null

    companion object {
        const val REPOSITORY = "REPOSITORY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repository)
        supportActionBar?.title = "Pull Requests"

        if (intent.extras[REPOSITORY] != null) {
            repository = intent.extras[REPOSITORY] as Repository
        }

        if (savedInstanceState != null) {
            repositoryFragment = supportFragmentManager
                    .getFragment(savedInstanceState, RepositoryFragment.TAG) as RepositoryFragment
        }

        if (repositoryFragment == null) {
            repositoryFragment = RepositoryFragment()
            loadFragment()
        } else {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, repositoryFragment, RepositoryFragment.TAG)
                    .commit()
        }
    }

    override fun loadFragment() {
        supportFragmentManager.beginTransaction()
                .add(R.id.container, repositoryFragment, RepositoryFragment.TAG).commit()
    }

    override fun getRepository(): Repository {
        return repository
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        supportFragmentManager.putFragment(outState, RepositoryFragment.TAG, repositoryFragment)
    }
}
