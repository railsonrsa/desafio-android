package com.example.railsonalmeida.concrete.application.di

import com.example.railsonalmeida.concrete.view.repositories.di.RepositoriesComponent
import com.example.railsonalmeida.concrete.view.repositories.di.RepositoriesModule
import com.example.railsonalmeida.concrete.view.repository.di.RepositoryComponent
import com.example.railsonalmeida.concrete.view.repository.di.RepositoryModule
import dagger.Component
import javax.inject.Singleton

/**
 * Created by railsonalmeida on 15/03/2018.
 */

@Singleton
@Component(modules = [(AppModule::class), (NetworkModule::class), (ApiModule::class)])
interface AppComponent {

    fun inject(repositoriesModule: RepositoriesModule): RepositoriesComponent

    fun inject(repositoryModule: RepositoryModule): RepositoryComponent

}