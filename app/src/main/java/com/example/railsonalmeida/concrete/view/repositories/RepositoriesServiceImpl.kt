package com.example.railsonalmeida.concrete.view.repositories

import com.example.railsonalmeida.concrete.view.repositories.data.Repositories
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesApi
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesService
import io.reactivex.Observable

/**
 * Created by railsonalmeida on 15/03/2018.
 */

class RepositoriesServiceImpl(private val api: RepositoriesApi): RepositoriesService {

    override fun loadRepositories(page: Int): Observable<Repositories> {
        return api.loadRepositories(page)
    }

}