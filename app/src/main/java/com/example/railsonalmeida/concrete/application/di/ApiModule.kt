package com.example.railsonalmeida.concrete.application.di

import com.example.railsonalmeida.concrete.BuildConfig
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesApi
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by railsonalmeida on 15/03/2018.
 */

@Module
class ApiModule {

    @Provides
    @Singleton
    @Named("baseUrl")
    fun provideBaseUrl(): String {
        return BuildConfig.BASE_URL
    }

    @Provides
    @Singleton
    fun provideRepositoriesApi(retrofit: Retrofit): RepositoriesApi {
        return retrofit.create(RepositoriesApi::class.java)
    }

    @Provides
    @Singleton
    fun provideRepositoryApi(retrofit: Retrofit): RepositoryApi {
        return retrofit.create(RepositoryApi::class.java)
    }
}