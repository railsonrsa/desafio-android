package com.example.railsonalmeida.concrete.application.di

import javax.inject.Scope

/**
 * Created by railsonalmeida on 15/03/2018.
 */

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity