package com.example.railsonalmeida.concrete.application.di

import android.app.Application
import android.content.Context
import com.example.railsonalmeida.concrete.application.schedulers.AppSchedulers
import dagger.Module
import dagger.Provides
import java.io.File
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by railsonalmeida on 15/03/2018.
 */

@Module
class AppModule(private val application: Application) {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return application
    }

}