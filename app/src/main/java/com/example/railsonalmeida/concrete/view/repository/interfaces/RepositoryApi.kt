package com.example.railsonalmeida.concrete.view.repository.interfaces

import com.example.railsonalmeida.concrete.view.repository.data.PullRequest
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by railsonalmeida on 19/03/2018.
 */

interface RepositoryApi {

    @GET("repos/{owner}/{repoName}/pulls")
    fun loadRepositoryPullRequests(@Path("owner") owner: String,
                                   @Path("repoName") name: String): Observable<Array<PullRequest>>
}