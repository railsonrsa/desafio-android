package com.example.railsonalmeida.concrete.view.repositories.interfaces

import com.example.railsonalmeida.concrete.application.base.BaseView
import com.example.railsonalmeida.concrete.view.repository.data.Repository

/**
 * Created by railsonalmeida on 15/03/2018.
 */

interface RepositoriesView: BaseView {

    fun showLoadingRepositories()

    fun hideLoadingRepositories()

    fun setRepositoriesData(repositories: List<Repository>)
}