package com.example.railsonalmeida.concrete.application.base

import android.support.v7.app.AppCompatActivity

/**
 * Created by railsonalmeida on 15/03/2018.
 */

abstract class BaseActivity: AppCompatActivity() {

    abstract fun loadFragment()

}