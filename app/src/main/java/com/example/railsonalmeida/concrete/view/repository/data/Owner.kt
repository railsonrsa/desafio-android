package com.example.railsonalmeida.concrete.view.repository.data

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by railsonalmeida on 16/03/2018.
 */

data class Owner(@SerializedName("login") var login: String,
                 @SerializedName("avatar_url") var avatarUrl: String): Serializable