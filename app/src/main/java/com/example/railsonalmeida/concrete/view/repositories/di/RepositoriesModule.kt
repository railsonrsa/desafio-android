package com.example.railsonalmeida.concrete.view.repositories.di

import com.example.railsonalmeida.concrete.application.schedulers.AppSchedulers
import com.example.railsonalmeida.concrete.application.schedulers.SchedulerProvider
import com.example.railsonalmeida.concrete.view.repositories.RepositoriesPresenterImpl
import com.example.railsonalmeida.concrete.view.repositories.RepositoriesServiceImpl
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesApi
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesPresenter
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesService
import dagger.Module
import dagger.Provides

/**
 * Created by railsonalmeida on 15/03/2018.
 */

@Module
class RepositoriesModule {

    @Provides
    fun providePresenter(service: RepositoriesService,  schedulers: SchedulerProvider): RepositoriesPresenter {
        return RepositoriesPresenterImpl(null, service, schedulers)
    }

    @Provides
    fun provideService(api: RepositoriesApi): RepositoriesService {
        return RepositoriesServiceImpl(api)
    }

    @Provides
    fun provideAppScheduler(): SchedulerProvider {
        return AppSchedulers()
    }

}