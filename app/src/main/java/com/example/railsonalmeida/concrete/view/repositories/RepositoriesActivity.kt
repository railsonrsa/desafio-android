package com.example.railsonalmeida.concrete.view.repositories

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import com.example.railsonalmeida.concrete.R
import com.example.railsonalmeida.concrete.application.base.BaseActivity
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesCallback
import com.example.railsonalmeida.concrete.view.repository.RepositoryActivity
import com.example.railsonalmeida.concrete.view.repository.data.Repository
import java.io.Serializable

class RepositoriesActivity : BaseActivity(), RepositoriesCallback {

    private var repositoriesFragment: RepositoriesFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repositories)
        supportActionBar?.title = "Repositories"

        if (savedInstanceState != null) {
            repositoriesFragment = supportFragmentManager
                    .getFragment(savedInstanceState, RepositoriesFragment.TAG) as RepositoriesFragment
        }

        if (repositoriesFragment == null) {
            repositoriesFragment = RepositoriesFragment()
            loadFragment()
        } else {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, repositoriesFragment, RepositoriesFragment.TAG)
                    .commit()
        }

    }

    override fun loadFragment() {
        supportFragmentManager.beginTransaction()
                .add(R.id.container, repositoriesFragment, RepositoriesFragment.TAG)
                .commit()
    }

    override fun openRepositoryPullRequests(repository: Repository) {
        val intent = Intent(this, RepositoryActivity::class.java)
        val bundle = Bundle()
        bundle.putSerializable(RepositoryActivity.REPOSITORY, repository as Serializable)
        intent.putExtras(bundle)
        startActivity(intent)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        supportFragmentManager.putFragment(outState, RepositoriesFragment.TAG, repositoriesFragment)
    }
}
