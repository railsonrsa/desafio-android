package com.example.railsonalmeida.concrete.view.repository.data

import com.google.gson.annotations.SerializedName

/**
 * Created by railsonalmeida on 19/03/2018.
 */

data class PullRequest(@SerializedName("state") val state: String,
                       @SerializedName("title") val title: String,
                       @SerializedName("user") val user: Owner,
                       @SerializedName("body") val body: String)