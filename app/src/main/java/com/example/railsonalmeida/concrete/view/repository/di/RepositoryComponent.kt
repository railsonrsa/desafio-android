package com.example.railsonalmeida.concrete.view.repository.di

import com.example.railsonalmeida.concrete.application.di.PerActivity
import com.example.railsonalmeida.concrete.view.repository.RepositoryFragment
import dagger.Subcomponent

/**
 * Created by railsonalmeida on 19/03/2018.
 */

@PerActivity
@Subcomponent(modules = [(RepositoryModule::class)])
interface RepositoryComponent {

    fun inject(target: RepositoryFragment)

}