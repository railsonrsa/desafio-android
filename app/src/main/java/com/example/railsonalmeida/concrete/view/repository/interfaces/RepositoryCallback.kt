package com.example.railsonalmeida.concrete.view.repository.interfaces

import com.example.railsonalmeida.concrete.view.repository.data.Repository

/**
 * Created by railsonalmeida on 19/03/2018.
 */

interface RepositoryCallback {

    fun getRepository(): Repository

}