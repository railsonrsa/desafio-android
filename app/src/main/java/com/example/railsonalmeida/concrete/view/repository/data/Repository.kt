package com.example.railsonalmeida.concrete.view.repository.data

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by railsonalmeida on 15/03/2018.
 */

data class Repository(@SerializedName("name") var name: String,
                      @SerializedName("full_name") var fullName: String,
                      @SerializedName("owner") var owner: Owner,
                      @SerializedName("description") var description: String,
                      @SerializedName("stargazers_count") var stargazersCount: Int,
                      @SerializedName("forks_count") var forks_count: Int): Serializable