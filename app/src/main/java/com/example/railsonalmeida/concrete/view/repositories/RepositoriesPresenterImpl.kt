package com.example.railsonalmeida.concrete.view.repositories

import android.util.Log
import com.example.railsonalmeida.concrete.application.base.BaseView
import com.example.railsonalmeida.concrete.application.schedulers.SchedulerProvider
import com.example.railsonalmeida.concrete.view.repositories.data.Repositories
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesPresenter
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesService
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by railsonalmeida on 15/03/2018.
 */

class RepositoriesPresenterImpl(private var view: RepositoriesView?,
                                private val service: RepositoriesService,
                                private val schedulers: SchedulerProvider): RepositoriesPresenter {

    companion object {
        const val TAG = "REPOSITORIES_PRESENTER"
    }

    private val disposables = CompositeDisposable()

    override fun setView(view: BaseView) {
        this.view = view as RepositoriesView
    }

    override fun loadRepositories(page: Int) {
        view?.showLoadingRepositories()

        disposables.add(service.loadRepositories(page)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe(
                        {response -> onSuccessLoadRepositories(response)  },
                        {error -> onErrorLoadRepositories(error) }
                )
        )
    }

    private fun onSuccessLoadRepositories(response: Repositories) {
        if (response.items.isNotEmpty()) {
            view?.setRepositoriesData(response.items)
        } else {
            Log.i(TAG, "Empty Repositories!")
        }
        view?.hideLoadingRepositories()
    }

    private fun onErrorLoadRepositories(error: Throwable?) {
//        Log.e(TAG, error.toString())
        view?.hideLoadingRepositories()
    }

    override fun destroyDisposables() {
        disposables.dispose()
    }
}