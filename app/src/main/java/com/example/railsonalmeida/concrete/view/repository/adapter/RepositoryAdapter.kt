package com.example.railsonalmeida.concrete.view.repository.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.railsonalmeida.concrete.R
import com.example.railsonalmeida.concrete.application.utils.PicassoCircleTransform
import com.example.railsonalmeida.concrete.view.repository.data.PullRequest
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_view_pullrequest.view.*

/**
 * Created by railsonalmeida on 19/03/2018.
 */

class RepositoryAdapter(): RecyclerView.Adapter<RepositoryViewHolder>() {

    private lateinit var context: Context
    private var data: List<PullRequest> = ArrayList()

    private var inflater: LayoutInflater? = null

    constructor(context:Context) : this() {
        this.context = context
        inflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RepositoryViewHolder {
        val view = inflater?.inflate(R.layout.item_view_pullrequest, parent, false)
        return RepositoryViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RepositoryViewHolder?, position: Int) {
        val pullRequest = data[position]
        Picasso.with(context).load(pullRequest.user.avatarUrl)
                .transform(PicassoCircleTransform()).into(holder?.avatar)
        holder?.pullTitle?.text = pullRequest.title
        holder?.pullBody?.text = pullRequest.body
        holder?.pullOwnerName?.text = pullRequest.user.login
    }

    fun addAll(pullRequests: List<PullRequest>) {
        data = pullRequests
    }

    fun getAll(): ArrayList<PullRequest> {
        return this.data as ArrayList<PullRequest>
    }

}

class RepositoryViewHolder(itemView: View?): RecyclerView.ViewHolder(itemView) {
    var avatar = itemView?.iv_owner_avatar
    var pullTitle = itemView?.tv_pull_title
    var pullBody = itemView?.tv_pull_body
    var pullOwnerName = itemView?.tv_pull_owner_name
}