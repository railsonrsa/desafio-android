package com.example.railsonalmeida.concrete.view.repositories.interfaces

import com.example.railsonalmeida.concrete.view.repository.data.Repository

/**
 * Created by railsonalmeida on 19/03/2018.
 */

interface RepositoriesCallback {

    fun openRepositoryPullRequests(repository: Repository)

}