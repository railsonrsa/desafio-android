package com.example.railsonalmeida.concrete.view.repositories

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.railsonalmeida.concrete.R
import com.example.railsonalmeida.concrete.application.ConcreteApplication
import com.example.railsonalmeida.concrete.application.base.BaseFragment
import com.example.railsonalmeida.concrete.application.utils.EndlessScrollListener
import com.example.railsonalmeida.concrete.application.utils.OnListItemClickListener
import com.example.railsonalmeida.concrete.view.repositories.adapter.RepositoriesAdapter
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesCallback
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesPresenter
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesView
import com.example.railsonalmeida.concrete.view.repository.data.Repository
import kotlinx.android.synthetic.main.fragment_repositories.*
import java.io.Serializable
import javax.inject.Inject

/**
 * Created by railsonalmeida on 15/03/2018.
 */

class RepositoriesFragment: BaseFragment(), RepositoriesView {

    private var repositories = ArrayList<Repository>()
    private lateinit var adapter: RepositoriesAdapter

    private lateinit var scrollListener: EndlessScrollListener
    private lateinit var callback: RepositoriesCallback

    private var pageNum = 1

    companion object {
        const val TAG = "REPOSITORIES_FRAGMENT"
        const val DATA = "DATA"
        const val PAGE = "PAGE"

    }

    @Inject lateinit var presenter: RepositoriesPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (context.applicationContext as ConcreteApplication).repositoriesComponent.inject(this)

        if (savedInstanceState != null) {
            repositories =  savedInstanceState.get(DATA) as ArrayList<Repository>
            pageNum = savedInstanceState.get(PAGE) as Int
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_repositories, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        callback = context as RepositoriesCallback
        setView()

        adapter = RepositoriesAdapter(this.context, repositories, object: OnListItemClickListener {
            override fun onItemClicked(repository: Repository) {
                callback.openRepositoryPullRequests(repository)
            }
        })
        val linearLayoutManager = LinearLayoutManager(this.context)

        rv_repositories.adapter = adapter
        rv_repositories.layoutManager = linearLayoutManager
        rv_repositories.addItemDecoration(DividerItemDecoration(rv_repositories.context,
                linearLayoutManager.orientation))

        scrollListener = object: EndlessScrollListener(linearLayoutManager, pageNum, 5){
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
                pageNum = page
                presenter.loadRepositories(pageNum)
            }
        }
        rv_repositories.addOnScrollListener(scrollListener)

        if (savedInstanceState != null) {
            setRepositoriesData(repositories)
        } else {
            presenter.loadRepositories(pageNum)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.destroyDisposables()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putSerializable(DATA, adapter.getAll() as Serializable)
        outState?.putSerializable(PAGE, pageNum)
    }

    override fun setView() {
        presenter.setView(this)
    }

    override fun showLoadingRepositories() {
        if (pageNum == 1) {
            loading.visibility = View.VISIBLE
        }
    }

    override fun hideLoadingRepositories() {
        loading.visibility = View.GONE
    }

    override fun setRepositoriesData(repositories: List<Repository>) {
        adapter.addAll(repositories)
        adapter.notifyDataSetChanged()
    }
}