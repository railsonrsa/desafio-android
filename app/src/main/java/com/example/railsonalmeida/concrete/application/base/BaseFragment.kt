package com.example.railsonalmeida.concrete.application.base

import android.support.v4.app.Fragment

/**
 * Created by railsonalmeida on 15/03/2018.
 */

abstract class BaseFragment: Fragment() {

    abstract fun setView()

}