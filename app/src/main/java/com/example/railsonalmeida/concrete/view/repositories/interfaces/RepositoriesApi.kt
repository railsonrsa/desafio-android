package com.example.railsonalmeida.concrete.view.repositories.interfaces

import com.example.railsonalmeida.concrete.view.repositories.data.Repositories
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by railsonalmeida on 15/03/2018.
 */

interface RepositoriesApi {

    @GET("/search/repositories?q=language:Java&sort=stars")
    fun loadRepositories(@Query("page")page: Int): Observable<Repositories>
}