package com.example.railsonalmeida.concrete.view.repository

import com.example.railsonalmeida.concrete.view.repository.data.PullRequest
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryApi
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryService
import io.reactivex.Observable

/**
 * Created by railsonalmeida on 19/03/2018.
 */

class RepositoryServiceImpl(private val api: RepositoryApi): RepositoryService {

    override fun loadRepositoryPullRequests(owner: String, name: String): Observable<Array<PullRequest>> {
        return api.loadRepositoryPullRequests(owner, name)
    }
}