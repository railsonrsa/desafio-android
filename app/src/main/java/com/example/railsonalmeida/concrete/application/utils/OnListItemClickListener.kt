package com.example.railsonalmeida.concrete.application.utils

import com.example.railsonalmeida.concrete.view.repository.data.Repository

/**
 * Created by railsonalmeida on 19/03/2018.
 */

interface OnListItemClickListener {

    fun onItemClicked(repository: Repository)

}