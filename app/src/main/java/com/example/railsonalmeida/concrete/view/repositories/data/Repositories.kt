package com.example.railsonalmeida.concrete.view.repositories.data

import com.example.railsonalmeida.concrete.view.repository.data.Repository
import com.google.gson.annotations.SerializedName

/**
 * Created by railsonalmeida on 15/03/2018.
 */

data class Repositories(@SerializedName("items") var items: List<Repository>)