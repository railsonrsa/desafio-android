package com.example.railsonalmeida.concrete.view.repositories.interfaces

import com.example.railsonalmeida.concrete.application.base.BasePresenter

/**
 * Created by railsonalmeida on 15/03/2018.
 */

interface RepositoriesPresenter: BasePresenter {

    fun loadRepositories(page: Int)

    fun destroyDisposables()

}