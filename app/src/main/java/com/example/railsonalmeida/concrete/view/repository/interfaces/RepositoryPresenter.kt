package com.example.railsonalmeida.concrete.view.repository.interfaces

import com.example.railsonalmeida.concrete.view.repository.data.PullRequest
import com.example.railsonalmeida.concrete.view.repository.data.Repository

/**
 * Created by railsonalmeida on 19/03/2018.
 */

interface RepositoryPresenter {

    fun setView(view: RepositoryView)

    fun loadRepositoryPullRequests(repository: Repository)

    fun onSuccessLoadRepositoryPullRequests(pullRequests: Array<PullRequest>)

    fun onErrorLoadRepositoryPullRequests(error: Throwable?)

    fun destroyDiposables()

}