package com.example.railsonalmeida.concrete.view.repositories.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.railsonalmeida.concrete.R
import com.example.railsonalmeida.concrete.application.utils.OnListItemClickListener
import com.example.railsonalmeida.concrete.application.utils.PicassoCircleTransform
import com.example.railsonalmeida.concrete.view.repository.data.Repository
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_view_repository.view.*

/**
 * Created by railsonalmeida on 15/03/2018.
 */

class RepositoriesAdapter(): RecyclerView.Adapter<RepositoriesViewHolder>() {

    private lateinit var context: Context
    private lateinit var data: MutableList<Repository>
    private lateinit var clickListener: OnListItemClickListener

    private var inflater: LayoutInflater? = null

    constructor(context: Context, repositories: List<Repository>, clickListener: OnListItemClickListener) : this() {
        this.context = context
        this.data = repositories as MutableList<Repository>
        this.clickListener = clickListener

        inflater = LayoutInflater.from(context)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RepositoriesViewHolder {
        val view = inflater?.inflate(R.layout.item_view_repository, parent, false)
        return RepositoriesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RepositoriesViewHolder?, position: Int) {
        val repository = data[position]
        Picasso.with(context).load(repository.owner.avatarUrl)
                .transform(PicassoCircleTransform()).into(holder?.avatar)
        holder?.fullName?.text = repository.fullName
        holder?.description?.text = repository.description
        holder?.forksCount?.text = repository.forks_count.toString()
        holder?.starsCount?.text = repository.stargazersCount.toString()
        holder?.bind(clickListener, repository)
    }

    fun addAll(repositories: List<Repository>) {
        this.data.addAll(repositories)
    }

    fun getAll(): List<Repository> {
        return this.data
    }
}

class RepositoriesViewHolder(itemView: View?): RecyclerView.ViewHolder(itemView) {
    var avatar = itemView?.iv_avatar
    var fullName = itemView?.tv_full_name
    var description = itemView?.tv_description
    var forksCount = itemView?.tv_repo_forks_count
    var starsCount = itemView?.tv_repo_stars_count

    fun bind(clickListener: OnListItemClickListener, repository: Repository) {
        itemView.setOnClickListener { clickListener.onItemClicked(repository) }
    }

}