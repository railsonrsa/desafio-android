package com.example.railsonalmeida.concrete.view.repository

import com.example.railsonalmeida.concrete.application.schedulers.SchedulerProvider
import com.example.railsonalmeida.concrete.view.repository.data.PullRequest
import com.example.railsonalmeida.concrete.view.repository.data.Repository
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryPresenter
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryService
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryView
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by railsonalmeida on 19/03/2018.
 */

class RepositoryPresenterImpl(private var view: RepositoryView?,
                              private val service: RepositoryService,
                              private val schedulers: SchedulerProvider): RepositoryPresenter {

    private val disposables = CompositeDisposable()

    companion object {
        const val TAG = "REPOSITORY_PRESENTER"
    }

    override fun setView(view: RepositoryView) {
        this.view = view
    }

    override fun loadRepositoryPullRequests(repository: Repository) {
        view?.showLoading()

        disposables.add(service.loadRepositoryPullRequests(repository.owner.login, repository.name)
                .observeOn(schedulers.ui())
                .subscribeOn(schedulers.io())
                .subscribe(
                        { pullRequests -> onSuccessLoadRepositoryPullRequests(pullRequests)  },
                        { error -> onErrorLoadRepositoryPullRequests(error)}
                )
        )
    }

    override fun onSuccessLoadRepositoryPullRequests(pullRequests: Array<PullRequest>) {
        if (pullRequests.isNotEmpty()) {
            view?.setPullRequestsData(pullRequests.toList())
        } else {
            view?.showEmptyPullRequests()
        }
        view?.hideLoading()
    }

    override fun onErrorLoadRepositoryPullRequests(error: Throwable?) {
//        Log.e(TAG, error.toString())
        view?.hideLoading()
    }

    override fun destroyDiposables() {
        disposables.dispose()
    }
}