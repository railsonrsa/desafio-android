package com.example.railsonalmeida.concrete.application.schedulers

import io.reactivex.Scheduler

/**
 * Created by railsonalmeida on 20/03/2018.
 */

interface SchedulerProvider {

    fun io(): Scheduler

    fun ui(): Scheduler

}