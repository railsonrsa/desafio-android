package com.example.railsonalmeida.concrete.view.repositories.di

import com.example.railsonalmeida.concrete.application.di.PerActivity
import com.example.railsonalmeida.concrete.view.repositories.RepositoriesFragment
import dagger.Subcomponent

/**
 * Created by railsonalmeida on 15/03/2018.
 */

@PerActivity
@Subcomponent(modules = [(RepositoriesModule::class)])
interface RepositoriesComponent {

    fun inject(target: RepositoriesFragment)

}