package com.example.railsonalmeida.concrete.view.repositories

import com.example.railsonalmeida.concrete.application.schedulers.TestSchedulers
import com.example.railsonalmeida.concrete.view.repositories.data.Repositories
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesPresenter
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesService
import com.example.railsonalmeida.concrete.view.repositories.interfaces.RepositoriesView
import com.example.railsonalmeida.concrete.view.repository.data.Owner
import com.example.railsonalmeida.concrete.view.repository.data.Repository
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by railsonalmeida on 21/03/2018.
 */
@RunWith(MockitoJUnitRunner::class)
class RepositoriesPresenterImplTest {

    @Mock lateinit var view: RepositoriesView
    @Mock lateinit var service: RepositoriesService
    lateinit var presenter: RepositoriesPresenter

    lateinit var repositories: Repositories
    var listRepositories = ArrayList<Repository>()

    @Before
    fun setUp() {

        presenter = RepositoriesPresenterImpl(view, service, TestSchedulers())
        presenter.setView(view)

        for(i in 1..5) {
            val owner = Owner("login $i", "url $i")
            val repository = Repository("Name $i", "Full name $i", owner,
                    "Description $i", i, i)
            listRepositories.add(repository)
        }

        repositories = Repositories(listRepositories)

    }

    @Test
    fun noInteractionOnViewIfPresenterNotCalled() {
        Mockito.verifyZeroInteractions(view)
    }

    @Test
    fun onSuccessLoadRepositories() {
        `when`(service.loadRepositories(1)).thenReturn(Observable.just(repositories))

        presenter.loadRepositories(1)

        verify(view, times(1)).showLoadingRepositories()
        verify(view, times(1)).setRepositoriesData(repositories.items)
        verify(view, times(1)).hideLoadingRepositories()
    }

    @Test
    fun onErrorLoadRepositories() {
        `when`(service.loadRepositories(1)).thenReturn(Observable.error(Throwable()))

        presenter.loadRepositories(1)

        verify(view, times(1)).showLoadingRepositories()
        verify(view, times(1)).hideLoadingRepositories()
    }

}