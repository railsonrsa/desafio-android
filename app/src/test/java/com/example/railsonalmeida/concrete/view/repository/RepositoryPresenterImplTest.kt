package com.example.railsonalmeida.concrete.view.repository

import com.example.railsonalmeida.concrete.application.schedulers.TestSchedulers
import com.example.railsonalmeida.concrete.view.repository.data.Owner
import com.example.railsonalmeida.concrete.view.repository.data.PullRequest
import com.example.railsonalmeida.concrete.view.repository.data.Repository
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryPresenter
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryService
import com.example.railsonalmeida.concrete.view.repository.interfaces.RepositoryView
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by railsonalmeida on 21/03/2018.
 */
@RunWith(MockitoJUnitRunner::class)
class RepositoryPresenterImplTest {

    @Mock lateinit var view: RepositoryView
    @Mock lateinit var service: RepositoryService
    lateinit var presenter: RepositoryPresenter

    lateinit var pullRequests: Array<PullRequest>
    lateinit var owner: Owner
    lateinit var repository: Repository

    @Before
    fun setUp() {

        presenter = RepositoryPresenterImpl(view, service, TestSchedulers())
        presenter.setView(view)

        val data = ArrayList<PullRequest>()
        for(i in 1..5) {
            owner = Owner("login $i", "url $i")
            val pullRequest = PullRequest("State $i", "Title $i", owner, "Body $i")
            data.add(pullRequest)
        }
        pullRequests =  Array(data.size, { i -> data[i] })

        repository = Repository("Name", "Full name", owner,
                "Description", 1, 1)

    }

    @Test
    fun noInteractionOnViewIfPresenterNotCalled() {
        Mockito.verifyNoMoreInteractions(view)
    }

    @Test
    fun onSuccessLoadPullRequests() {
      `when`(service.loadRepositoryPullRequests(ArgumentMatchers.anyString(), ArgumentMatchers.anyString()))
              .thenReturn(Observable.just(pullRequests))

        presenter.loadRepositoryPullRequests(repository)

        verify(view, times(1)).showLoading()
        verify(view, times(1)).setPullRequestsData(pullRequests.asList())
        verify(view, times(1)).hideLoading()

    }

    @Test
    fun onSuccessLoadPullRequestsEmpty() {
        `when`(service.loadRepositoryPullRequests(ArgumentMatchers.anyString(), ArgumentMatchers.anyString()))
                .thenReturn(Observable.just(emptyArray()))

        presenter.loadRepositoryPullRequests(repository)

        verify(view, times(1)).showLoading()
        verify(view, times(1)).showEmptyPullRequests()
        verify(view, never()).setPullRequestsData(pullRequests.asList())
        verify(view, times(1)).hideLoading()
    }

    @Test
    fun OnErrorLoadPullRequests() {
        `when`(service.loadRepositoryPullRequests(ArgumentMatchers.anyString(), ArgumentMatchers.anyString()))
                .thenReturn(Observable.error(Throwable()))

        presenter.loadRepositoryPullRequests(repository)

        verify(view, times(1)).showLoading()
        verify(view, times(1)).hideLoading()
    }
}